import { Injectable } from '@nestjs/common';
import { startUnleash } from 'unleash-client';

@Injectable()
export class FeatureFlagService {
  private unleashClient: any;

  constructor() {
    this.unleashClient = this.intitUnlishClient();
  }

  private async intitUnlishClient(): Promise<any> {
    return await startUnleash({
      url: process.env.UNLEASH_URL,
      appName: 'backend-service',
      instanceId: process.env.UNLEASH_INSTANCE_ID,
    });
  }

  private async getUnleashClient(): Promise<any> {
    if (this.unleashClient) return this.unleashClient;

    this.unleashClient = await this.intitUnlishClient();

    return this.unleashClient;
  }

  public async isFrenchEnabled(): Promise<boolean> {
    return (await this.getUnleashClient()).isEnabled('french');
  }
}
