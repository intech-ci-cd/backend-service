import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FeatureFlagModule } from './feature-flag/feature-flag.module';

@Module({
  imports: [FeatureFlagModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
