import { Injectable } from '@nestjs/common';
import { FeatureFlagService } from './feature-flag/feature-flag.service';

@Injectable()
export class AppService {
  constructor(private readonly featureFlag: FeatureFlagService) {}

  async getHello(): Promise<string> {
    if (await this.featureFlag.isFrenchEnabled()) {
      return 'Bonjour tout le Monde!';
    } else {
      return 'Hello World!';
    }
  }

  async healthz(): Promise<string> {
    return 'Backend Service is healthy!';
  }
}
