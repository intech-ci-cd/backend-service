import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FeatureFlagModule } from './feature-flag/feature-flag.module';
import { FeatureFlagService } from './feature-flag/feature-flag.service';

describe('AppService', () => {
  let appService: AppService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [],
      providers: [
        AppService,
        FeatureFlagService,
        {
            provide: FeatureFlagService,
            useValue: {
                    isFrenchEnabled: jest.fn().mockResolvedValue(false),
            },
        },
      ],
    }).compile();

    appService = app.get<AppService>(AppService);
  });
  
  describe('root', () => {
    it('should return "Hello World!"', async () => {
      await expect(appService.getHello()).resolves.toBe('Hello World!');
    });
  });

  describe('healthz', () => {
    it('should return "Backend Service is healthy!"', async () => {
      await expect(appService.healthz()).resolves.toBe('Backend Service is healthy!');
    });
  });
});
